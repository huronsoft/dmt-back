package co.com.dmt.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import co.com.dmt.entity.Warehouses;
import co.com.dmt.service.WareHouseService;


@ExtendWith(MockitoExtension.class)
public class WareHouseControllersTest {
	
	@InjectMocks
	WareHouseController wareHouseController;
	
	@Mock
	WareHouseService service;

	@Test
	public void findAllTest() throws Exception{
		Warehouses warehouse1 = new Warehouses(1l, "68546854sdf-d465-w5496454", "nominal s.a.s", 10);
		Warehouses warehouse2 = new Warehouses(1l, "85546854sdf-d465-w5496587", "perfectos ltda", 20);
		List<Warehouses> warehouses = new ArrayList<Warehouses>();
		warehouses.add(warehouse1);
		warehouses.add(warehouse2);
		
		when(service.findWarehouses()).thenReturn(warehouses);
		List<Warehouses> result = wareHouseController.findAll();
		assertThat(result.size()).isEqualTo(2);
	}
}
