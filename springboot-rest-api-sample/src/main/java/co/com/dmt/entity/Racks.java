package co.com.dmt.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "racks")
@NamedQueries({
    @NamedQuery(name = "Racks.findAll", query = "SELECT r FROM Racks r"),
    @NamedQuery(name = "Racks.findById", query = "SELECT r FROM Racks r WHERE r.id = :id"),
    @NamedQuery(name = "Racks.findByUuid", query = "SELECT r FROM Racks r WHERE r.uuid = :uuid"),
    @NamedQuery(name = "Racks.findByTypeRack", query = "SELECT r FROM Racks r WHERE r.typeRack = :typeRack")})
public class Racks implements Serializable {
	private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @Column(name = "uuid")
    private String uuid;
    @Column(name = "type_rack")
    private Character typeRack;
    @Column(name = "warehouse_id")
    private Long warehouseId;
//    @JoinColumn(name = "warehouse_id", referencedColumnName = "id")
//    @ManyToOne(fetch = FetchType.LAZY)
//    private Warehouses warehouseId;

    public Racks() {
    }

    public Racks(Long id, String uuid) {
        this.id = id;
        this.uuid = uuid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Character getTypeRack() {
        return typeRack;
    }

    public void setTypeRack(Character typeRack) {
        this.typeRack = typeRack;
    }

//    public Warehouses getWarehouseId() {
//        return warehouseId;
//    }
//
//    public void setWarehouseId(Warehouses warehouseId) {
//        this.warehouseId = warehouseId;
//    }

    public Long getWarehouseId() {
		return warehouseId;
	}

	public void setWarehouseId(Long warehouseId) {
		this.warehouseId = warehouseId;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof Racks)) {
//            return false;
//        }
//        Racks other = (Racks) object;
//        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
//            return false;
//        }
//        return true;
//    }
//
//    @Override
//    public String toString() {
//        return "co.com.dmt.Racks[ id=" + id + " ]";
//    }
}
