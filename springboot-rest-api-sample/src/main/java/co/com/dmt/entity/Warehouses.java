/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.com.dmt.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "warehouses")
@NamedQueries({
    @NamedQuery(name = "Warehouses.findAll", query = "SELECT w FROM Warehouses w"),
    @NamedQuery(name = "Warehouses.findById", query = "SELECT w FROM Warehouses w WHERE w.id = :id"),
    @NamedQuery(name = "Warehouses.findByUuid", query = "SELECT w FROM Warehouses w WHERE w.uuid = :uuid"),
    @NamedQuery(name = "Warehouses.findByClientName", query = "SELECT w FROM Warehouses w WHERE w.clientName = :clientName"),
    @NamedQuery(name = "Warehouses.findByFamily", query = "SELECT w FROM Warehouses w WHERE w.family = :family"),
    @NamedQuery(name = "Warehouses.findBySize", query = "SELECT w FROM Warehouses w WHERE w.size = :size")})
public class Warehouses implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @Column(name = "uuid")
    private String uuid;
    @Basic(optional = false)
    @Column(name = "client_name")
    private String clientName;
    @Column(name = "family")
    private String family;
    @Basic(optional = false)
    @Column(name = "size")
    private int size;
    //@OneToMany(fetch = FetchType.LAZY)
    @OneToMany(mappedBy = "warehouseId", fetch = FetchType.LAZY)
    private List<Racks> racksList;

    public Warehouses() {
    }

    public Warehouses(Long id, String uuid, String clientName, int size) {
        this.id = id;
        this.uuid = uuid;
        this.clientName = clientName;
        this.size = size;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public List<Racks> getRacksList() {
        return racksList;
    }

    public void setRacksList(List<Racks> racksList) {
        this.racksList = racksList;
    }

//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (id != null ? id.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof Warehouses)) {
//            return false;
//        }
//        Warehouses other = (Warehouses) object;
//        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
//            return false;
//        }
//        return true;
//    }
//
//    @Override
//    public String toString() {
//        return "co.com.dmt.Warehouses[ id=" + id + " ]";
//    }
    
}
