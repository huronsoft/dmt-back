package co.com.dmt.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.dmt.entity.Racks;
import co.com.dmt.entity.Warehouses;
import co.com.dmt.repository.RackRepository;
import co.com.dmt.repository.WareHouseRepository;
import co.com.dmt.util.UtilUuid;

@Service
public class WareHouseService {

	@Autowired 
	private WareHouseRepository wareHouseRepository;
	
	@Autowired 
	private RackRepository rackRepository;
	
	public List<Warehouses> findWarehouses(){
		return wareHouseRepository.findAll();
	}
	
	public List<Racks> findByWareHouseId(Long warehouseId){
		return rackRepository.findByWareHouseId(warehouseId);
	}
	
	@Transactional
	public void save(Warehouses warehouses) {
		warehouses.setUuid(UtilUuid.generateUUIIDCode());
		wareHouseRepository.save(warehouses);
	}
	
	@Transactional
	public void update(Warehouses warehouses) {
		wareHouseRepository.save(warehouses);
	}
	
	@Transactional
	public void saveRack(Racks rack) {
		rack.setUuid(UtilUuid.generateUUIIDCode());
		rackRepository.save(rack);
	}
	
	@Transactional
	public void deleteWareHouse(Long id) {
		wareHouseRepository.deleteById(id);
	}
	
	@Transactional
	public void deleteRack(Racks rack) {
		rackRepository.delete(rack);
	}
}
