package co.com.dmt.dto;

import java.io.Serializable;
import java.util.List;

public class WarehousesDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6889467175705524375L;
	
	private Integer id;
	
	private String uuid;
	
	private String clientName;
	
	private String family;
	
	private int size;
	
	private List<RacksDTO> racksList;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getFamily() {
		return family;
	}

	public void setFamily(String family) {
		this.family = family;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public List<RacksDTO> getRacksList() {
		return racksList;
	}

	public void setRacksList(List<RacksDTO> racksList) {
		this.racksList = racksList;
	}
}
