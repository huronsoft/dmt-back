package co.com.dmt.dto;

import java.io.Serializable;

public class RacksDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2337636567078540500L;
	
	private Integer id;
	
	private String uuid;
	
	private Character typeRack;
	
	private Integer warehouseId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Character getTypeRack() {
		return typeRack;
	}

	public void setTypeRack(Character typeRack) {
		this.typeRack = typeRack;
	}

	public Integer getWarehouseId() {
		return warehouseId;
	}

	public void setWarehouseId(Integer warehouseId) {
		this.warehouseId = warehouseId;
	}
}
