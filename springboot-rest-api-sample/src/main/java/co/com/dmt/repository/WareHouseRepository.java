package co.com.dmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.com.dmt.entity.Warehouses;

@Repository
public interface WareHouseRepository extends JpaRepository<Warehouses, Long>{
	
	
}
