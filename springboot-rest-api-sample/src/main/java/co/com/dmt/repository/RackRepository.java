package co.com.dmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import co.com.dmt.entity.Racks;

@Repository
public interface RackRepository extends JpaRepository<Racks, Long>{

	@Query(value="select * from racks r "
			+ "where r.warehouse_id = :warehouseId", nativeQuery=true)
	public List<Racks> findByWareHouseId(@Param(value = "warehouseId") Long warehouseId);
}
