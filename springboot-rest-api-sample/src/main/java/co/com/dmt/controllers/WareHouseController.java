package co.com.dmt.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import co.com.dmt.entity.Racks;
import co.com.dmt.entity.Warehouses;
import co.com.dmt.service.WareHouseService;

/**
 *
 * A sample greetings controller to return greeting text
 */
@RestController
@RequestMapping("/")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.PUT,RequestMethod.POST})
public class WareHouseController {
	
	@Autowired
	private WareHouseService wareHouseService;
    
    @RequestMapping(value = "findall", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @CrossOrigin(origins = "http://localhost:4200")
    @ResponseStatus(HttpStatus.OK)
    public List<Warehouses> findAll(){
    	return wareHouseService.findWarehouses();
    }
    
    @RequestMapping(value = "findbywarehouseid/{warehouseId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @CrossOrigin(origins = "http://localhost:4200")
    @ResponseStatus(HttpStatus.OK)
    public List<Racks> findByWareHouseId(@PathVariable Long warehouseId){
    	return wareHouseService.findByWareHouseId(warehouseId);
    }
    
    @RequestMapping(value = "save", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @CrossOrigin(origins = "http://localhost:4200")
    @ResponseStatus(HttpStatus.OK)
    public void saveWareHouse(@RequestBody Warehouses warehouse){
    	wareHouseService.save(warehouse);
    }
    
    @RequestMapping(value = "update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    @CrossOrigin(origins = "http://localhost:4200")
    @ResponseStatus(HttpStatus.OK)
    public void updateWareHouse(@RequestBody Warehouses warehouse){
    	wareHouseService.update(warehouse);
    }
    
    @RequestMapping(value = "rack", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @CrossOrigin(origins = "http://localhost:4200")
    @ResponseStatus(HttpStatus.OK)
    public void saveRack(@RequestBody Racks rack){
    	wareHouseService.saveRack(rack);
    }
    
    @RequestMapping(value = "deleterack", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @CrossOrigin(origins = "http://localhost:4200")
    @ResponseStatus(HttpStatus.OK)
    public void deleteRack(@RequestBody Racks racks){
    	wareHouseService.deleteRack(racks);
    }
    
    @RequestMapping(value = "delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @CrossOrigin(origins = "http://localhost:4200")
    @ResponseStatus(HttpStatus.OK)
    public void deleteWareHouse(@RequestBody Warehouses warehouse){
    	wareHouseService.deleteWareHouse(warehouse.getId());
    }
    
    
}
