package co.com.dmt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 *
 * Spring Boot application starter class
 */
@SpringBootApplication
@ComponentScan(basePackages = {"co.com.dmt.controllers", "co.com.dmt.service", "co.com.dmt.repository"})
@EntityScan("co.com.dmt.entity")
@EnableJpaRepositories("co.com.dmt.repository")
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
